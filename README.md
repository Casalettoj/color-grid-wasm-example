chroma-holder

Small color grid placeholder to demonstrate very, very basic wasm bindgen use.

Requires:  

- https://www.rust-lang.org/tools/install Rust and cargo
- https://github.com/sagiegurari/cargo-make cargo-make
- python or python3


Instructions:

```bash
cargo make wasm-bindgen
python3 -m http.server 8000
```

The color grid should display at localhost:8000
