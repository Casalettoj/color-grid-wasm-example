pub mod color {
    pub type R = u8;
    pub type G = u8;
    pub type B = u8;
    pub type A = u8;

    #[derive(Clone, Copy, Debug, PartialEq)]
    pub struct Color(pub R, pub G, pub B, pub A);

    impl Color {
        pub fn get_rgba(&self) -> (&R, &G, &B, &A) {
            (&self.0, &self.1, &self.2, &self.3)
        }
    }
}

pub mod coordinates {
    pub type X = u64;
    pub type Y = u64;

    #[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
    pub struct CartesianCoordinate {
        pub x: X,
        pub y: Y,
    }
}

pub mod grid {
    use super::{color::Color, coordinates::CartesianCoordinate};
    use std::collections::HashMap;

    pub struct ColorGrid {
        pub width: u64,
        pub height: u64,
        pub colors: HashMap<CartesianCoordinate, Color>,
    }

    impl ColorGrid {
        pub fn new(width: u64, height: u64) -> Self {
            let mut colors = HashMap::new();
            for x in 0..width {
                for y in 0..height {
                    colors.insert(
                        CartesianCoordinate { x, y },
                        Color(
                            ((y * 255) / height) as u8,
                            ((x * 255) / width) as u8,
                            (((width - y) * 255) / height) as u8,
                            255,
                        ),
                    );
                }
            }
            Self {
                width,
                height,
                colors,
            }
        }
    }
}
